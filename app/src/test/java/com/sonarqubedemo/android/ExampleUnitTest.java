package com.sonarqubedemo.android;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {


    @Test
    public void addition_isCorrect() {
        someClass aa = new someClass();
        int sum = aa.sum(2,2);
        assertEquals(sum, 4);
    }

    @Test
    public void minus_isCorrect() {
        someClass aa = new someClass();
        int sum = aa.minus(2,2);
        assertEquals(sum, 0);
    }
}